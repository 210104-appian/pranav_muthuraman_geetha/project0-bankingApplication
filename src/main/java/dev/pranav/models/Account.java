package dev.pranav.models;

public class Account {
	private int id;
	private double balance;
	private String status;//ACTIVE, PENDING or REJECTED. REJECTED HAS NO BALANCE BUT STILL EXISTS FOR RECORD KEEPING
	private String type;//Savings or Checking
	
	
	public Account() {
		// TODO Auto-generated constructor stub
	}
	
	public Account(int id, double balance, String status, String type) {
		this.id = id;
		this.balance = balance;
		this.status = status;
		this.type = type;
	}
	
	
	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

}

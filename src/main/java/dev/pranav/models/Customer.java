package dev.pranav.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dev.pranav.daos.UserDao;

public class Customer extends User {
	
	private int creditScore;
	private List<Account> accounts = new ArrayList<>();
	//Constructors
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(String name, String username, String password, int id, int creditScore, List<Account> accounts) {//made w/o accounts list
		super(name, username, password, id);
		this.creditScore = creditScore;
		this.accounts = accounts;
	}
	
	//methods for customer menu
	
	public void viewDetails() {
		System.out.printf("%-15s %-20s %-20s %-20s\n", "Customer ID", "Name", "Username",  "Credit Score");
		System.out.printf("%-15s %-20s %-20s %-20s\n", this.id, this.name, this.username, this.creditScore);
	}
	
	public void viewAccounts() {
		if(accounts.isEmpty()) {
			System.out.println("No accounts associated with this user.");
		}else {
			System.out.printf("%-15s %-20s %-20s %-20s\n", "Account ID", "Status", "Type",  "Balance");
		}
		for (Account account : accounts) {
			System.out.printf("%-15s %-20s %-20s %-20s\n", account.getId(), account.getStatus(), account.getType(), account.getBalance());
		}
	}
	
	public boolean accountExists(int accID) {//checks if account exists w account ID
		for (Account account : accounts) {
			if (account.getId() == accID) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean hasActiveAccount() {
		for (Account account : this.accounts) {
			if (account.getStatus().equals("ACTIVE"));
			return true;
		}
		return false;
	}
	
	public boolean isActiveAccount(int accID) {
		for(Account account : accounts) {
			if(account.getId() == accID && account.getStatus().equals("ACTIVE")) {
				return true;
			}
		}
		return false;
	}
	
	public boolean depositMoney(double deposit, int accID, UserDao user) {
		double total;
		if (BigDecimal.valueOf(deposit).scale() > 2 || deposit < 0) {
			return false;
		}else {
			for(Account account : accounts) {
				if(account.getId() == accID) {
					total = deposit + account.getBalance();
					if (total > 9999999999.99) {
						System.out.println("WOAH THERE! That is way too much money! Please deposit a smaller amount");
						return false;
					}
					else {
						if(user.updateAccountBalance(accID, total)) {
							account.setBalance(total);
							int transactionID = user.transactionIDNum();
							user.addTransaction(transactionID, deposit, "DEPOSIT", accID);
							System.out.println("Successfully deposited!\n Returning to menu");
							return true;
						}else {
							System.out.println("Something went wrong. Please re-enter your deposit amount.");//THIS SHOULD NEVER RUN if it does something is wrong w accID or total
							return false;
						}
					}
				}
			}
			return false;//This means it exited out of the for loop without matching the account ID, this should never happen
		}
	}
	
	public boolean withdrawMoney(double withdrawal, int accID, UserDao user) {
		double newBalance;
		double currentBalance;
		if (BigDecimal.valueOf(withdrawal).scale() > 2 || withdrawal < 0) {
			return false;
		}else {
			for(Account account : accounts) {
				if(account.getId() == accID) {
					currentBalance = account.getBalance();
					if(withdrawal > currentBalance) {
						System.out.println("The withdrawal amount is larger than the balance.");
						System.out.println("The most you can withdraw is " + currentBalance);
					}else {
						newBalance = currentBalance - withdrawal;
						if(user.updateAccountBalance(accID, newBalance)) {
							account.setBalance(newBalance);
							int transactionID = user.transactionIDNum();
							user.addTransaction(transactionID, withdrawal, "WITHDRAWAL", accID);
							System.out.println("Succesfully withdrew!\nReturning to menu");
							return true;
						}else {
							System.out.println("Something went wrong. Please re-enter your withdrawal amount.");
							return false;
						}
					}
				}
			}
			return false;//This means it exited out of the for loop without matching the account ID, this should never happen
		}
	}
	
	public boolean createNewAccount(Scanner ls, UserDao user) {
		Account newAccount = new Account();
		String type = new String();
		int customerID = this.id;
		double balance;
		System.out.println("Enter the type of account you would like to apply for\n(c) Checking\n(s) Savings");
		char accTypeChar = ls.next().toLowerCase().charAt(0);
		while(accTypeChar != 'c' && accTypeChar != 's') {
			System.out.println("Invalid Entry. Please enter again");
			accTypeChar = ls.next().toLowerCase().charAt(0);
		}
		if(accTypeChar == 'c') {
			type = "CHECKING";
		}else {
			type = "SAVINGS";
		}
		System.out.println("Please enter your starting balance");
		while(!ls.hasNextDouble()) {
			System.out.println("Invalid Entry. Please enter a numerical value");
			ls.next();
		}
		balance = ls.nextDouble();
		while(BigDecimal.valueOf(balance).scale() > 2 || balance < 0) {
			System.out.println("Invalid Entry. Please enter a non-negative value precise up to 2 decimal places");
			while(!ls.hasNextDouble()) {
				System.out.println("Invalid Entry. Please enter a numerical value");
				ls.next();
			}
			balance = ls.nextDouble();
		}
		int accID = user.accountIDNum();
		if(user.newAcount(balance, type, customerID, accID)) {
			newAccount.setBalance(balance);
			newAccount.setId(accID);
			newAccount.setStatus("PENDING");
			newAccount.setType(type);
			this.accounts.add(newAccount);
			System.out.println("Successfully applied for the account!\nOnce an employee approves this account then you may make transactions with this account");
			return true;
		}else {
			System.out.println("Unable to create this account. Returning to menu");//should not reach this code
			return false;
		}		
	}

	//Getters & Setters
	
	public int getCreditScore() {
		return creditScore;
	}

	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

}

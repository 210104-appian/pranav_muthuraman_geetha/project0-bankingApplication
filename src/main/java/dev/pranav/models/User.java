package dev.pranav.models;

public abstract class User {
	protected String name;
	protected String username;
	protected String password;
	protected int id;
	
	//Constructors
	
	public User() {
	}
	
	public User(String name, String username, String password, int id) {
		this.setName(name);
		this.setUsername(username);
		this.setPassword(password);
		this.setId(id);
	}
	
	//Getters & Setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

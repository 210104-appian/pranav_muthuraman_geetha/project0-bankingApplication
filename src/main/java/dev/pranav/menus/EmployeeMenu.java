package dev.pranav.menus;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dev.pranav.actions.Verification;
import dev.pranav.daos.UserDao;
import dev.pranav.models.Account;
import dev.pranav.models.Customer;
import dev.pranav.models.Employee;

public class EmployeeMenu {
	Customer c1 = new Customer();
	List<Account> accounts= new ArrayList<>();
	
	public EmployeeMenu() {
		// TODO Auto-generated constructor stub
	}
	
	public void employeeOptions(UserDao user, Verification action, Employee e1, Scanner ls) {
		System.out.println("Hello! What would you like to do today?");
		boolean runOnce = false;
		char switchMenu = 0;
		while(switchMenu != 'l') {
			if(runOnce) {
				System.out.println("\nWhat else would you like to do today?");
			}
			System.out.println("(d) View User Details\n(c) View a Customer's Accounts\n(p) Approve/Reject Pending Accounts\n(t) View an Account's Transactions\n(l) Logout");
			switchMenu = ls.next().toLowerCase().charAt(0);
			while(switchMenu != 'd' && switchMenu != 'c' && switchMenu != 'p' && switchMenu != 't' && switchMenu != 'l' ) {
				System.out.println("Invalid Entry. Please enter again");
				switchMenu = ls.next().toLowerCase().charAt(0);
			}
			runOnce = true;
			switch(switchMenu) {
				case 'd': e1.viewDetails();
					break;
				case 'c': System.out.println("This is a list of all customers");
					user.seeAllCustomers();
					System.out.println("To see a customer's accounts enter their id");
					while(!ls.hasNextInt()) {
						System.out.println("Please input a valid Customer ID");
						ls.next();
					}
					int customerID = ls.nextInt();
					while(!user.customerExists(customerID)) {
						System.out.println("No customer accounts belong to that number");
						while(!ls.hasNextInt()) {
							System.out.println("Please input a valid Customer ID");
						}
						customerID = ls.nextInt();
					}
					user.createCustomer(customerID, c1);
					user.addCustomerAccts(c1);
					c1.viewAccounts();
					break;
				case 'p': accounts = user.seeAllPending(); 
					if(accounts.isEmpty()) {
					System.out.println("No pending accounts at this time");
					break;
				}else {
					System.out.println("Select an account to approve/reject by entering the account number");
					while(!ls.hasNextInt()) {
						System.out.println("Please input a valid Account ID");
						ls.next();
					}
					int accID = ls.nextInt();
					boolean containsID = false;
					for (Account account : accounts) {
						if(account.getId() == accID) {
							containsID = true;
						}
					}
					while (!containsID) {
						System.out.println("There are no accounts with this ID that are pending.\nPlease enter a different account ID");
						while(!ls.hasNextInt()) {
							System.out.println("Please input a valid Account ID");
							ls.next();
						}
						accID = ls.nextInt();
						for (Account account : accounts) {
							if(account.getId() == accID) {
								containsID = true;
							}
						}
					}
					System.out.println("Would you like to approve or reject Account #" + accID + "?\n(a) Approve\n(r) Reject");
					char approvalChoice = ls.next().toLowerCase().charAt(0); 	
					while( approvalChoice != 'a' && approvalChoice != 'r' ) {
							System.out.println("Invalid entry. Please enter again");
							approvalChoice = ls.next().toLowerCase().charAt(0);
						}
					if(approvalChoice == 'a') {
						user.updateStatus(accID, "ACTIVE");
					}else {
						user.updateStatus(accID, "REJECTED");
					}
				}
					
					break;
				case 't': accounts = user.seeAllAccounts();
					System.out.println("Please input the account ID of the account which you would like to view the transactions");
					while(!ls.hasNextInt()) {
						System.out.println("Please input a valid Account ID");
						ls.next();
					}
					int accID = ls.nextInt();
					boolean containsID = false;
					for (Account account : accounts) {
						if(account.getId() == accID) {
							containsID = true;
						}
					}
					while (!containsID) {
						System.out.println("There are no accounts with this ID.\nPlease enter an account ID");
						while(!ls.hasNextInt()) {
							System.out.println("Please input a valid Account ID");
							ls.next();
						}
						accID = ls.nextInt();
						for (Account account : accounts) {
							if(account.getId() == accID) {
								containsID = true;
							}
						}
					}
					user.seeTransactions(accID);
					break;
			
			}
		}
	}
}

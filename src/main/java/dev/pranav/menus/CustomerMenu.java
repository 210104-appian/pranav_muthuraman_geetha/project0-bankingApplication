package dev.pranav.menus;

import java.util.Scanner;

import dev.pranav.actions.Verification;
import dev.pranav.daos.UserDao;
import dev.pranav.models.Customer;

public class CustomerMenu {

	public CustomerMenu() {
		// TODO Auto-generated constructor stub
	}
	
	public void customerOptions(UserDao user, Verification action, Customer c1, Scanner ls) {
		System.out.println("Welcome! How may we help you today?");
		boolean runOnce = false;
		char switchMenu = 0;
		while(switchMenu != 'l') {
			if(runOnce) {
				System.out.println("\nHow else may we help you today?");
			}
			System.out.println("(d) View User Details\n(v) View List of Accounts\n(m) Make a Withdrawal or Deposit\n(a) Apply for an account\n(l) Logout");
			String customerSelection = ls.next();
			customerSelection = customerSelection.toLowerCase();
			while(customerSelection.charAt(0) != 'd' && customerSelection.charAt(0) != 'v' && customerSelection.charAt(0) != 'l' && customerSelection.charAt(0) != 'm' && customerSelection.charAt(0) != 'a') {
				System.out.println("Invalid entry. Please enter again");
				customerSelection = ls.next();
				customerSelection = customerSelection.toLowerCase();
			}
			runOnce = true;
			switchMenu = customerSelection.charAt(0);
			switch(switchMenu) {
				case 'd': c1.viewDetails();
					break;
				case 'v': c1.viewAccounts();
					break;
				case 'm': 
					if(!c1.hasActiveAccount()) {
						System.out.println("You have no active accounts.\nPlease open an account first to make a transaction.");
						break;
					}
					System.out.println("Please enter the account ID of which you would like to make a transaction");
					while(!ls.hasNextInt()) {
						System.out.println("Please input a valid Account ID comprised of just integers");
						ls.next();
					}
					int accID = ls.nextInt();//this is an int but might not be an account

					char returnChar = 0;
					outerloop:
					while(!c1.accountExists(accID)) {
						System.out.println("No accounts with that ID exist in your name\nPlease re-enter the account number or press (b) to return to the previous menu");
						while(!ls.hasNextInt()) {
							returnChar = ls.next().toLowerCase().charAt(0); 
							if (returnChar == 'b')
							{
								break outerloop;
							}else {
							System.out.println("Please input a valid Account ID comprised of just integers or press (b) to return");
							}
						}
						accID = ls.nextInt();
					}
					//accID is now an account but might not be active or they are returning to menu
					

					if(returnChar != 'b') {
					outerloop:
						while(!c1.isActiveAccount(accID)) {
							System.out.println("That account is not active. Please enter a different account ID or press (b) to return");
							
							while(!ls.hasNextInt()) {
								returnChar = ls.next().toLowerCase().charAt(0); 
								if (returnChar == 'b')
								{
									break outerloop;
								}else {
								System.out.println("Please input a valid Account ID comprised of just integers or press (b) to return");
								}
							}
							accID = ls.nextInt();//this is an int
								while(!c1.accountExists(accID)) {
									System.out.println("No accounts with that ID exist in your name\nPlease re-enter the account number or press (b) to return to the previous menu");
									while(!ls.hasNextInt()) {
										returnChar = ls.next().toLowerCase().charAt(0); 
										if (returnChar == 'b')
										{
											break outerloop;
										}else {
										System.out.println("Please input a valid Account ID comprised of just integers or press (b) to return");
										}
									}
									accID = ls.nextInt();//this is an int
								}
							
						}
					}
					
					if(c1.isActiveAccount(accID)) {
						System.out.println("Would you like to make a withdrawal or deposit?\n(w) Withdrawal\n(d) Deposit\n(c) Cancel Transaction");
						char transactionType = ls.next().toLowerCase().charAt(0);
						while (transactionType != 'w' && transactionType != 'd' && transactionType != 'c') {
							System.out.println("Invalid entry. Please enter again.");
							transactionType = ls.next().toLowerCase().charAt(0);
						}
						if(transactionType == 'c') {
							System.out.println("Transaction cancelled. Returning to previous menu.");
							break;
						}
						else if(transactionType == 'd') {
							System.out.println("How much would you like to deposit?");
							while(!ls.hasNextDouble()) {
								System.out.println("Invalid Entry. Please enter a numeric value.");
								ls.next();
							}
							double deposit = ls.nextDouble();
							while (!c1.depositMoney(deposit, accID, user)) {
								System.out.println("Invalid amount. Please enter a non-negative value precise up to 2 decimal places");
								while(!ls.hasNextDouble()) {
									System.out.println("Invalid Entry. Please enter a numeric value.");
									ls.next();
								}
								deposit = ls.nextDouble();
							}
							break;
						}
						else {//Withdrawal case no other option
								System.out.println("How much would you like to withdraw");
								while(!ls.hasNextDouble()) {
									System.out.println("Invalid Entry. Please enter a numeric value.");
									ls.next();
								}
								double withdrawal = ls.nextDouble();
								while (!c1.withdrawMoney(withdrawal, accID, user)) {
									System.out.println("Invalid amount. Please enter a non-negative value precise up to 2 decimal places");
									while(!ls.hasNextDouble()) {
										System.out.println("Invalid Entry. Please enter a numeric value.");
										ls.next();
									}
									withdrawal = ls.nextDouble(); 
								}
							break;
						}
					}
					
				case 'a': c1.createNewAccount(ls, user);
					break;
				}
		
			}
		}
	}


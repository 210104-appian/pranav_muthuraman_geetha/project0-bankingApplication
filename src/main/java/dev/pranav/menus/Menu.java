package dev.pranav.menus;

import java.util.Scanner;

import dev.pranav.daos.UserDao;
import dev.pranav.daos.UserDaoImpl;
import dev.pranav.models.*;
import dev.pranav.actions.*;

public class Menu {

	char runApplication = 'y';
	UserDao user = new UserDaoImpl();
	Verification action = new Verification();
	Customer c1 = new Customer();
	Employee e1 = new Employee();
	EmployeeMenu em1 = new EmployeeMenu();
	CustomerMenu cu2 = new CustomerMenu();
	
	
	public Menu() {
		
	}
	
	public void loginMenu(){
		System.out.println("Hello. To begin please indicate whether you are:\n(c) A Customer\n(e) An Employee\n(q) Quit Application");
		Scanner ls = new Scanner(System.in);
		String loginSelection = ls.next();
		loginSelection = loginSelection.toLowerCase();
		
		while(loginSelection.charAt(0) != 'e' && loginSelection.charAt(0) != 'c' && loginSelection.charAt(0) != 'q') {
			System.out.println("Invalid entry. Please enter again");
			loginSelection = ls.next();
			loginSelection = loginSelection.toLowerCase();
		}
		//ls.close(); throws a NoSuchElement Exception when method is called again in driver, don't close scanner.
		
		
		
		char switchCorE = loginSelection.charAt(0);
		switch(switchCorE) {
			case 'e':
				//take us to employee login verification meaning call verification method
//				System.out.println("Please enter your username");//**TAKE A LOOK NOT PERMANENT** Probably move to verification method
//				String username = ls.next();//Does hold onto username, nothing to change
				if(action.verifyEmployee(ls, user, e1)) {
					System.out.println("Login successful");
					em1.employeeOptions(user, action, e1, ls);
				}
				break;
			case 'c':
				if(action.verifyCustomer(ls, user, c1)) {
					System.out.println("Login successful");
					cu2.customerOptions(user, action, c1, ls);
				}
				//take us to customer login verification
				break;
			case 'q':
				runApplication = 'n';
				break;
		}
	}

	
		
		
		
		//Getter
		public char getRunApplication() {
			return runApplication;
		}

}

package dev.pranav.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.pranav.models.Account;
import dev.pranav.models.Customer;
import dev.pranav.models.Employee;
import dev.pranav.models.User;
import dev.pranav.util.ConnectionUtil;

public class UserDaoImpl implements UserDao {

	private static Logger log = Logger.getRootLogger();
	
	@Override
	public String getEmployeePassword(String employeeUserName) {//FINISH THIS METHOD
		String sql = "SELECT PASSWORD FROM EMPLOYEE WHERE USERNAME = ?";
		String password = new String();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
					pStatement.setString(1, employeeUserName);
					ResultSet resultSet = pStatement.executeQuery();
					if(!resultSet.next()) {
						return null;
					}
//					resultSet.next();
					password = resultSet.getString("PASSWORD");//Works
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password;
	}
	
	
	@Override
	public String getCustomerPassword(String customerUserName) {
		String sql = "SELECT PASSWORD FROM CUSTOMER WHERE USERNAME = ?";
		String password = new String();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
					pStatement.setString(1, customerUserName);
					ResultSet resultSet = pStatement.executeQuery();
					if(!resultSet.next()) {
						return null;
					}
//					resultSet.next();
					password = resultSet.getString("PASSWORD");//Works
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password;
	}
	

	@Override
	public void createCustomer(String customerUserName, Customer c1) { //Only every called after customer username & password is verified!!
		String sql = "SELECT * FROM CUSTOMER WHERE USERNAME = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setString(1, customerUserName);
			ResultSet resultSet = pStatement.executeQuery();
			
			resultSet.next();
			c1.setId(resultSet.getInt("ID"));
			c1.setName(resultSet.getNString("NAME"));
			c1.setUsername(resultSet.getNString("USERNAME"));
			c1.setPassword(resultSet.getNString("PASSWORD"));
			c1.setCreditScore(resultSet.getInt("CREDIT_SCORE"));			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	
	@Override
	public void createCustomer(int customerID, Customer c1) {
		String sql = "SELECT * FROM CUSTOMER WHERE ID = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, customerID);
			ResultSet resultSet = pStatement.executeQuery();
			
			resultSet.next();
			c1.setId(resultSet.getInt("ID"));
			c1.setName(resultSet.getNString("NAME"));
			c1.setUsername(resultSet.getNString("USERNAME"));
			c1.setPassword(resultSet.getNString("PASSWORD"));
			c1.setCreditScore(resultSet.getInt("CREDIT_SCORE"));			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	
	
	@Override
	public boolean customerExists(int customerID) {
		String sql = "SELECT * FROM CUSTOMER WHERE ID = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, customerID);
			ResultSet resultSet = pStatement.executeQuery();
			
			if(resultSet.next()) {
				return true;
			}
						
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public void createEmployee(String employeeUserName, Employee e1) {
		String sql = "SELECT * FROM EMPLOYEE WHERE USERNAME = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setString(1, employeeUserName);
			ResultSet resultSet = pStatement.executeQuery();
			
			resultSet.next();
			e1.setId(resultSet.getInt("ID"));
			e1.setName(resultSet.getNString("NAME"));
			e1.setUsername(resultSet.getNString("USERNAME"));
			e1.setPassword(resultSet.getNString("PASSWORD"));			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	@Override
	public void seeAllCustomers() {//for employee to view all customers, doesn't include customer password - sensitive info
		log.info("Printing all customers");
		List<Customer> customers = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();	
				Statement statement = connection.createStatement();) {
			
			ResultSet resultSet = statement.executeQuery("SELECT * FROM CUSTOMER");
			while(resultSet.next()) {
				Customer customer = new Customer();
				customer.setName(resultSet.getNString("NAME"));
				customer.setCreditScore(resultSet.getInt("CREDIT_SCORE"));
				customer.setId(resultSet.getInt("ID"));
				customer.setUsername(resultSet.getNString("USERNAME"));
				customers.add(customer);
			}
			
		} catch (SQLException e) {
		
			log.error("Exception was thrown" + e);
		}
		
		//     \/   Important for formatting \/
		
		System.out.printf("%-15s %-20s %-20s %-20s\n", "Customer ID", "Name", "Username", "Credit Score");
		for (Customer customer : customers) {
			System.out.printf("%-15s %-20s %-20s %-20s\n", customer.getId(), customer.getName(), customer.getUsername(), customer.getCreditScore());
		}
		
	}

	
	
	@Override
	public void addCustomerAccts(Customer c1) {
		List<Account> accounts = new ArrayList<>();
		String sql = "SELECT * FROM ACCOUNT WHERE CUSTOMER_ID = ? ORDER BY ID ASC";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, c1.getId());
			ResultSet resultSet = pStatement.executeQuery();
			while(resultSet.next()) {
				Account a1 = new Account();
				a1.setBalance(resultSet.getDouble("BALANCE"));
				a1.setId(resultSet.getInt("ID"));
				a1.setStatus(resultSet.getNString("STATUS"));
				a1.setType(resultSet.getNString("ACCT_TYPE"));
				accounts.add(a1);
			}
			c1.setAccounts(accounts);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}


	@Override
	public boolean updateAccountBalance(int accID, double balance) {
		log.info("Made a withdrawal/deposit");
		String sql = "UPDATE ACCOUNT SET BALANCE = ? WHERE ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setDouble(1, balance);
			pStatement.setInt(2, accID);
			pStatement.execute();
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			log.error("Threw an exception " + e);
		}
		return false;
	}


	@Override
	public int transactionIDNum() {
		int index = 1;
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery("SELECT MAX(ID) FROM ACCT_TRANSACTION");
			if (!resultSet.next()) {
				index = 1;
				return index;
			}
			else {
				index = resultSet.getInt(1) + 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return index;
	}


	@Override
	public void addTransaction(int transactionID, double amount, String type, int accID) {
		log.info("logging transaction in transaction table");
		String sql = "INSERT INTO ACCT_TRANSACTION VALUES ( ?, ?, ?, ?)";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, transactionID);
			pStatement.setString(2, type);
			pStatement.setDouble(3, amount);
			pStatement.setInt(4, accID);
			pStatement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
	}


	@Override
	public boolean newAcount(double amount, String type, int customerID, int accID) {
		String sql = "INSERT INTO ACCOUNT VALUES ( ?, ?, 'PENDING', ?, ?) ";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, accID);
			pStatement.setDouble(2, amount);
			pStatement.setInt(3, customerID);
			pStatement.setString(4, type);
			pStatement.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	@Override
	public int accountIDNum() {
		int index = 1;
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery("SELECT MAX(ID) FROM ACCOUNT");
			if (!resultSet.next()) {
				index = 1;
				return index;
			}
			else {
				index = resultSet.getInt(1) + 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return index;
	}


	@Override
	public List<Account> seeAllPending() {
		List<Account> accounts = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();	
				Statement statement = connection.createStatement();){
			ResultSet resultSet = statement.executeQuery("SELECT NAME, a.ID AS ACCOUNT_ID, ACCT_TYPE, BALANCE FROM CUSTOMER c RIGHT JOIN ACCOUNT a ON c.ID = a.CUSTOMER_ID WHERE a.STATUS = 'PENDING' ORDER BY ACCOUNT_ID");
				while(resultSet.next()) {
					Account account = new Account();
					account.setBalance(resultSet.getDouble("BALANCE"));
					account.setType(resultSet.getNString("ACCT_TYPE"));
					account.setStatus(resultSet.getNString("NAME"));//Only using status to hold name here because name will only be called for printing it here
					account.setId(resultSet.getInt("ACCOUNT_ID"));
					
					accounts.add(account);
				}
				if(!accounts.isEmpty()) {
					System.out.println("These are the pending accounts");
				System.out.printf("%-20s %-20s %-20s %-20s\n", "Customer Name", "Account ID", "Account Type", "Starting Balance");
				}
				for (Account ac : accounts) {
					System.out.printf("%-20s %-20s %-20s %-20s\n", ac.getStatus(), ac.getId(), ac.getType(), ac.getBalance());
				}
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accounts;
		
	}


	@Override
	public void updateStatus(int accID, String status) {
		String sql = "UPDATE ACCOUNT SET STATUS = ? WHERE ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, status);
			pStatement.setInt(2, accID);
			pStatement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	@Override
	public List<Account> seeAllAccounts() {
		List<Account> accounts = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();	
				Statement statement = connection.createStatement();){
			ResultSet resultSet = statement.executeQuery("SELECT NAME, a.ID AS ACCOUNT_ID, ACCT_TYPE, BALANCE FROM CUSTOMER c RIGHT JOIN ACCOUNT a ON c.ID = a.CUSTOMER_ID ORDER BY ACCOUNT_ID");
				while(resultSet.next()) {
					Account account = new Account();
					account.setBalance(resultSet.getDouble("BALANCE"));
					account.setType(resultSet.getNString("ACCT_TYPE"));
					account.setStatus(resultSet.getNString("NAME"));//Only using status to hold name here because name will only be called for printing it here
					account.setId(resultSet.getInt("ACCOUNT_ID"));
					
					accounts.add(account);
				}
				if(!accounts.isEmpty()) {
					System.out.println("These are all of the accounts");
				System.out.printf("%-20s %-20s %-20s %-20s\n", "Customer Name", "Account ID", "Account Type", "Balance");
				}
				for (Account ac : accounts) {
					System.out.printf("%-20s %-20s %-20s %-20s\n", ac.getStatus(), ac.getId(), ac.getType(), ac.getBalance());
				}
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accounts;
		

	}


	@Override
	public void seeTransactions(int accID) {
		String sql = "SELECT ID, TRANSACTION_TYPE, AMOUNT FROM ACCT_TRANSACTION at2 WHERE at2.ACCOUNT_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, accID);
			ResultSet resultSet = pStatement.executeQuery();
			boolean ranOnce = false;
			while(resultSet.next()){
				if(!ranOnce) {
					System.out.printf("%-20s %-20s %-20s\n", "Transaction ID", "Transaction Type", "Amount");
				}
					System.out.printf("%-20s %-20s %-20s\n", resultSet.getInt("ID"), resultSet.getNString(2), resultSet.getDouble("AMOUNT"));
					ranOnce = true;
				}
			if(!ranOnce) {
				System.out.println("No transactions on this account");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

package dev.pranav.daos;

import java.util.List;

import dev.pranav.models.Account;
import dev.pranav.models.Customer;
import dev.pranav.models.Employee;
import dev.pranav.models.User;

public interface UserDao {
	public String getEmployeePassword(String employeeUserName);
	public String getCustomerPassword(String customerUserName);
	public void createCustomer(String customerUserName, Customer c1);
	public void createCustomer(int customerID, Customer c1);
	public boolean customerExists(int customerID);
	public void createEmployee(String employeeUserName, Employee e1);
	public void seeAllCustomers();
	public void addCustomerAccts(Customer c1);
	public boolean updateAccountBalance(int accID, double balance);
	public void addTransaction(int transactionID, double amount, String type, int accID);
	public int transactionIDNum();
	public boolean newAcount(double amount, String type, int customerID, int accID);
	public int accountIDNum();
	public List<Account> seeAllPending();//probably will be a join
	public void updateStatus(int accID, String Status);
	public List<Account> seeAllAccounts();
	public void seeTransactions(int accID);
}

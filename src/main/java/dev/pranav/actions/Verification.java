package dev.pranav.actions;

import java.util.Scanner;

import dev.pranav.daos.*;
import dev.pranav.models.*;

public class Verification {

	public String password = new String();

	
	public Verification() {
		// TODO Auto-generated constructor stub
	}
	
	public boolean verifyEmployee(Scanner ls, UserDao user, Employee e1) {
		int numAttempts = 5;
		password = null;
		System.out.println("Please enter your username");
		String name = ls.next();//****THIS WORKS, PASS IN SCANNER AS PARAM FOR METHODS IN THIS CLASS****
		password = user.getEmployeePassword(name);
		//verifying there is an account associated with this username
		while (password == null) {
			numAttempts--;
			if(numAttempts <= 0) {
				System.out.println("No attempts left. Returning to Start Menu.");
				return false;
			}
			System.out.println("Attempts remaining: " + numAttempts);
			System.out.println("No accounts associated with that username. Please re-enter your username");
			name = ls.next();
			password = user.getEmployeePassword(name);
		}
		
		//now actually verifying the password
		numAttempts = 5;
		System.out.println("Please enter your password");
		String pwAttempt = ls.next();
		while (!password.equals(pwAttempt)) {
			numAttempts--;
			if (numAttempts <= 0) {
				System.out.println("No attempts left. Returning to Start Menu.");
				return false;
			}
			System.out.println("Attempts remaining: " + numAttempts);
			System.out.println("Incorrect Password. Please re-enter your password.");
			pwAttempt = ls.next();
		}
		
		user.createEmployee(name, e1);
		return true;
	}
	
	public boolean verifyCustomer(Scanner ls, UserDao user, Customer c1) {
		int numAttempts = 5;
		password = null;
		System.out.println("Please enter your username");
		String name = ls.next();//****THIS WORKS, PASS IN SCANNER AS PARAM FOR METHODS IN THIS CLASS****
		password = user.getCustomerPassword(name);
		//verifying there is an account associated with this username
		while (password == null) {
			numAttempts--;
			if(numAttempts <= 0) {
				System.out.println("No attempts left. Returning to Start Menu.");
				return false;
			}
			System.out.println("Attempts remaining: " + numAttempts);
			System.out.println("No accounts associated with that username. Please re-enter your username");
			name = ls.next();
			password = user.getCustomerPassword(name);
		}
		
		//now actually verifying the password
		numAttempts = 5;
		System.out.println("Please enter your password");
		String pwAttempt = ls.next();
		while (!password.equals(pwAttempt)) {
			numAttempts--;
			if (numAttempts <= 0) {
				System.out.println("No attempts left. Returning to Start Menu.");
				return false;
			}
			System.out.println("Attempts remaining: " + numAttempts);
			System.out.println("Incorrect Password. Please re-enter your password.");
			pwAttempt = ls.next();
		}
		
		user.createCustomer(name, c1);
		user.addCustomerAccts(c1);
		return true;
	}
	
		
			

}

package dev.pranav.models;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;

public class CustomerTest {

		
		Account a1 = new Account(1, 500, "ACTIVE", "CHECKING");
		Account a2 = new Account(2, 500, "PENDING", "SAVINGS");
		Account a3 = new Account(3, 500, "REJECTED", "CHECKING");
		Account a4 = new Account(5, 500, "ACTIVE", "CHECKING");
		ArrayList<Account> accounts = new ArrayList<Account>(Arrays.asList(a1, a2, a3, a4));
		ArrayList<Account> accounts2 = new ArrayList<Account>(Arrays.asList(a2, a3));
		Customer c1 = new Customer("Johnny Lawrence", "JLAW", "Cobra", 1, 450, accounts);
		Customer c2 = new Customer("Ralph Macchio", "RMAC", "Miyagi", 2, 600, accounts2);
		Customer c3 = new Customer();
		
	@Test
	public void testWithNegativeValue() {
		boolean expected = false;
		boolean actual = c1.accountExists(-50);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithInbetweenValue() {
		boolean expected = false;
		boolean actual = c1.accountExists(4);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithNoAccountsInList() {
		boolean expected = false;
		boolean actual = c3.accountExists(1);
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void testWithNoAccounts() {
		boolean expected = false;
		boolean actual = c3.hasActiveAccount();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithOtherStatuses() {
		boolean expected = false;
		boolean actual = c2.hasActiveAccount();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithNegativeIDNumber() {
		boolean expected = false;
		boolean actual = c1.isActiveAccount(-22);
		assertEquals(expected, actual);
	}
}

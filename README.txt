README.txt

Introduction
-------------
The Banking Application provides an interface for both cutomers and employees of a bank handle interactions with their accounts. 

Configuration
--------------
In the pom.xml file you will need to ensure you have the following dependencies.
ojdc10 version 19.8.0.0
junit version 4.12
log4j version 1.2.17

the username for the 

Demo
-----
This application has been created with built-in users. There is no option to create new users through the console in this version of the application so it is necessary to know the usernames and passwords of the built-in users

Employees
-username : A.gamble123
-password : GATOR

-username : T.Hoitz56
-password : JETER

Customers
-username : SHEILA123
-password : GAMBLE456

-username : ALLEN101
-password : ARNIEPALMIE

There are some accounts created already however you as the user have the freedom to open more or approve/reject accounts. After logging in the application will show you the rest of the way!

Database username : mpranav23
password : p4ssv00rd
